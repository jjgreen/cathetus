#!/bin/sh
#
# refresh cathetus.* from git master

NAME=cathetus
URL="https://gitlab.com/jjg/$NAME/raw/master"

for ext in c h
do
    FILE="$NAME.$ext"
    echo -n "$FILE .."
    wget -q $URL/$FILE -O $FILE
    echo ". done"
done
