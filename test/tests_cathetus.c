/*
  tests_cathetus.h

  Copyright (c) J.J. Green 2018
*/

#include <cathetus.h>

#include <float.h>
#include <errno.h>
#include <fenv.h>

#include "tests_cathetus.h"
#include "gcc_version.h"

#ifndef DBL_TRUE_MIN
#define DBL_TRUE_MIN (DBL_MIN * DBL_EPSILON)
#endif

CU_TestInfo tests_cathetus[] = {
  {"denormal underflow", test_cathetus_denormal_underflow},
  {"simple underflow", test_cathetus_simple_underflow},
  {"simple overflow", test_cathetus_simple_overflow},
  {"infinite hypotenuse", test_cathetus_inf_hypot},
  {"nan hypotenuse", test_cathetus_nan_hypot},
  {"negative arguments", test_cathetus_negative},
  {"zero arguments", test_cathetus_zero},
  {"infeasible arguments", test_cathetus_infeasible},
  {"pythagorean triples", test_cathetus_triples},
  {"reflexivity", test_cathetus_reflexive},
  {"hypothesis regression", test_cathetus_hypothesis_regression},
  CU_TEST_INFO_NULL,
};

/* run cathetus checking errno and exceptions  */

static double cathetus_errno(double h, double a)
{
  errno = 0;
  feclearexcept(FE_ALL_EXCEPT);

  double b = cathetus(h, a);

  CU_ASSERT_EQUAL(errno, 0);
  CU_ASSERT_EQUAL(fetestexcept(FE_DIVBYZERO), 0);
  CU_ASSERT_EQUAL(fetestexcept(FE_OVERFLOW), 0);
  CU_ASSERT_EQUAL(fetestexcept(FE_UNDERFLOW), 0);

  /*
    For gcc-5.4.0, 5.5.0, 6.3.0 -O we get an FPE_INVALID
    for arguments inf, nan.  This does not occur for 4.8.4,
    7.3.0, nor for clang-3.8-O each with various values of
    -O, so I'm assuming an issue with gcc 5.*, 6.*  and for
    those gcc versions we skip this check.
  */

  if (fetestexcept(FE_INVALID) != 0)
    {
#if GCC_BETWEEN(50000, 70000)
      CU_PASS("gcc-5/6 spurious FE INVALID");
#else
      CU_FAIL("FE_INVALID");
#endif
    }

  return b;
}

/* assert expected finite value returned */

static void cathetus_assert_finite(double h, double a, double b)
{
  double b0 = cathetus_errno(h, a);
  CU_ASSERT_DOUBLE_EQUAL(b, b0, DBL_EPSILON * b0);
}

/* assert infinite value returned */

static void cathetus_assert_infinite(double h, double a)
{
  double b = cathetus_errno(h, a);
  CU_ASSERT(isinf(b));
  CU_ASSERT(b > 0);
}

/* assert NaN value returned */

static void cathetus_assert_nan(double h, double a)
{
  double b = cathetus_errno(h, a);
  CU_ASSERT(isnan(b));
}

/* assert of the same type (inf, nan, finite) */

static void cathetus_assert_type_equal(double a, double b)
{
  CU_ASSERT_EQUAL(fpclassify(a), fpclassify(b));
}

/* pythagorean multiple of smallest denormalised */

void test_cathetus_denormal_underflow(void)
{
  double
    h = 5 * DBL_TRUE_MIN,
    a = 4 * DBL_TRUE_MIN,
    b = cathetus_errno(h, a);

  CU_ASSERT_EQUAL(isnan(b), 0);
  CU_ASSERT(b > 0);
  CU_ASSERT_EQUAL(b, 3 * DBL_TRUE_MIN);
}

/*
  isosceles right triangle with cathetii DBL_MIN,
  the smallest normalised postive double, this will
  underflow a naive implementation
*/

void test_cathetus_simple_underflow(void)
{
  double
    a = DBL_MIN,
    h = a * sqrt(2),
    b = cathetus_errno(h, a);

  CU_ASSERT_EQUAL(isnan(b), 0);
  CU_ASSERT(b > 0);
  CU_ASSERT_DOUBLE_EQUAL(b, a, a * DBL_EPSILON);
}

/*
  isosceles right triangle with hypotenuse DBL_MAX,
  which will overflow in a naive implementation.
*/

void test_cathetus_simple_overflow(void)
{
  double
    h = DBL_MAX,
    a = h / sqrt(2),
    b = cathetus_errno(h, a);

  CU_ASSERT_EQUAL(isnan(b), 0);
  CU_ASSERT_EQUAL(isinf(b), 0);
}

/* NaN hypoteneuse */

void test_cathetus_nan_hypot(void)
{
  cathetus_assert_nan(NAN, 3);
  cathetus_assert_nan(NAN, -3);
  cathetus_assert_nan(NAN, 0);
  cathetus_assert_nan(NAN, INFINITY);
  cathetus_assert_nan(NAN, NAN);
}

/* infinite hypotenuse */

void test_cathetus_inf_hypot(void)
{
  cathetus_assert_infinite(INFINITY, 3);
  cathetus_assert_infinite(INFINITY, -3);
  cathetus_assert_infinite(INFINITY, 0);
  cathetus_assert_infinite(INFINITY, NAN);
  cathetus_assert_nan(INFINITY, INFINITY);
}

/*
  if the other side is larger than the hypotenuse
  (or if it is NaN) then return NaN
*/

void test_cathetus_infeasible(void)
{
  cathetus_assert_nan(2, 3);
  cathetus_assert_nan(2, -3);
  cathetus_assert_nan(2, INFINITY);
  cathetus_assert_nan(2, NAN);
}

/* negative arguments */

void test_cathetus_negative(void)
{
  cathetus_assert_finite(-5, 4, 3);
  cathetus_assert_finite(5, -4, 3);
  cathetus_assert_finite(-5, -4, 3);
}

/* zero argument edge-cases */

void test_cathetus_zero(void)
{
  cathetus_assert_finite(1, 0, 1);
  cathetus_assert_finite(0, 0, 0);
}

/* the primitive Pythagorean triples with hypotenuse under 300 */

void test_cathetus_triples(void)
{
  double triples[][3] = {
    {3, 4, 5},
    {5, 12, 13},
    {8, 15, 17},
    {7, 24, 25},
    {20, 21, 29},
    {12, 35, 37},
    {9, 40, 41},
    {28, 45, 53},
    {11, 60, 61},
    {16, 63, 65},
    {33, 56, 65},
    {48, 55, 73},
    {13, 84, 85},
    {36, 77, 85},
    {39, 80, 89},
    {65, 72, 97},
    {20, 99, 101},
    {60, 91, 109},
    {15, 112, 113},
    {44, 117, 125},
    {88, 105, 137},
    {17, 144, 145},
    {24, 143, 145},
    {51, 140, 149},
    {85, 132, 157},
    {119, 120, 169},
    {52, 165, 173},
    {19, 180, 181},
    {57, 176, 185},
    {104, 153, 185},
    {95, 168, 193},
    {28, 195, 197},
    {84, 187, 205},
    {133, 156, 205},
    {21, 220, 221},
    {140, 171, 221},
    {60, 221, 229},
    {105, 208, 233},
    {120, 209, 241},
    {32, 255, 257},
    {23, 264, 265},
    {96, 247, 265},
    {69, 260, 269},
    {115, 252, 277},
    {160, 231, 281},
    {161, 240, 289},
    {68, 285, 293}
  };
  size_t n = sizeof(triples) / (3 * sizeof(double));

  for (size_t i = 0 ; i < n ; i++)
    {
      double *triple = triples[i];
      double
        a = triple[0],
        b = triple[1],
        h = triple[2];

      cathetus_assert_finite(h, a, b);
    }
}

static void reflexive_check(double h, double a)
{
  double h0 = hypot(cathetus(h, a), a);
  cathetus_assert_type_equal(h, h0);
}

void test_cathetus_reflexive(void)
{
  reflexive_check(NAN, NAN);
  reflexive_check(NAN, 1.0);
  reflexive_check(NAN, 0.0);
  reflexive_check(INFINITY, NAN);
  reflexive_check(INFINITY, 1.0);
  reflexive_check(INFINITY, 0.0);
  reflexive_check(2.0, 1.0);
  reflexive_check(2.0, 0.0);
  reflexive_check(0.0, 0.0);
}

/*
  The hypotheis project uses a Python implementation of this
  function and reported a regression [1] whereby the returned
  value was larger than the hypotenuse. The same defect was
  found to be present here.

  [1] https://github.com/HypothesisWorks/hypothesis/pull/1772
*/

void test_cathetus_hypothesis_regression(void)
{
  double
    h = 6.28726416963871e-155,
    b = cathetus_errno(h, 0);

  CU_ASSERT(b <= h);
}
